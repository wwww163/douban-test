import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import homePage from '@/views/homePage'
import film from '@/views/film'
import books from '@/views/books'
import filmMore from '@/views/filmMore'
import filmDetail from '@/views/filmDetail'
import broadcast from '@/views/broadcast'
import searchBar from '@/views/searchPage'
import group from '@/views/group'
import DHeader from '@/components/header'
Vue.use(Router)

export default new Router({
  routes: [
    // routes,
    /*   {
        path: '/',
        name: 'Hello',
        component: HelloWorld
      }, */
      {
        path: '/',
        name: 'homePage',
        components: {
          default:homePage,
          header:DHeader }
        },
        {
          path: '/film',
          name: 'film',
          components:{
            default:film,//default对应的是主页面，filmPage对应的是组件
            filmPage:DHeader}
          },
          {
            path: '/books',
            name: 'books',
            components:{
              default:books,
              booksPage:DHeader
            }
          },
          {
            path: '/filmMore',
            name: 'filmMore',
            components:{
              default:filmMore,
              filmMorePage:DHeader
            }
          },
          {
            path: '/filmDetail/:id',
            name: 'filmDetail',
            components:{
              default:filmDetail,
              filmDetailPage:DHeader,

            }
          },
          {
            path: '/broadcast',
            name: 'broadcast',
            components:{
              default:broadcast,
              broadcastPage:DHeader
            }
          },
          {
            path: '/search',
            name: 'searchBar',
            component: searchBar
          },
          {
            path: '/group',
            name: 'group',
            components:{
              default:group,
              groupPage:DHeader
            }
          },
          ]})
