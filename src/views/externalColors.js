const colors={
	blue1:'rgb(35, 132, 232)',
	blue2:'rgb(35, 132, 232)',
	brown:'rgb(159, 120, 96)',
	red1:'rgb(230, 70, 126)',
	green1:'rgb(42, 184, 204)',
	yello:'rgb(42, 184, 204)',
	green2:'rgb(64, 207, 169)',
	green3:'rgb(66, 189, 86)',
	green4:'rgb(87, 116, 197)',
	green5:'rgb(89, 108, 221)',
}
export {colors};
